﻿/// <reference path="//ajax.googleapis.com/ajax/libs/angularjs/1.2.26/angular.min.js" />
/// <reference path="//ajax.googleapis.com/ajax/libs/angularjs/1.2.1/angular-route.min.js"/>
var app = angular.module('wigsternApp', []);

app.controller('homeController', function ($scope, $window) {
    $scope.NavigateToWigsPage = function(){
        $window.location.href='/wigs.html';
    }

    $scope.NavigateToSternPage = function(){
        $window.location.href='/stern.html';
    }
});

app.controller('wigsController', function ($scope) {
    $scope.message = "Wigs Page";




});

app.controller('sternController', function ($scope) {
    $scope.message = "Stern Page";

    $scope.Vote = function(imageType, imageName, numVotes){
    	var client = new WindowsAzure.MobileServiceClient(
		    "https://thewonderouswigs.azure-mobile.net/",
		    "mtcnXxtRCINdEjyoadIGoTJuyBUcQn75"
		);

		var item = { imagetype: imageType, imagename: imageName, votes: numVotes };
		client.getTable("imagevotes").insert(item);

    }

    $scope.GetVotes = function(imageName){
    	var client = new WindowsAzure.MobileServiceClient(
		    "https://thewonderouswigs.azure-mobile.net/",
		    "mtcnXxtRCINdEjyoadIGoTJuyBUcQn75"
		);

		var table = client.getTable("imagevotes").where({
			imagename: imageName
		}).read().done(function(results){
			//alert(JSON.stringify(results));

			//Sum the votes
			var totalVotes = 0;

			for (var i = 0; i < results.length; i++) {
				totalVotes = totalVotes + results[i].votes;
			}

			alert(imageName + ': ' + totalVotes + ' votes');

			//$scope.totalVotes = totalVotes;


		}, function(err){
			alert("Error: " + err);
		});

    }

});